package fr.example.demo_childprocess_vertx_ext;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.julienviet.childprocess.Process;
import com.julienviet.childprocess.ProcessOptions;

import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;

@ExtendWith(VertxExtension.class)
public class TestChildProcess {

  @Test
  void test(Vertx vertx, VertxTestContext testContext) throws Throwable {

    ProcessOptions options = new ProcessOptions();
    options.setCwd("/tmp");
    options.setEnv(Map.of("foo", "bar", "toto", "tata"));

    Process.create(vertx, "ls", List.of("-lh"), options)
      .startHandler(process -> {

        process.stdout().handler(buffOut -> System.out.println(buffOut.toString()));
        process.stderr().handler(buffer -> System.out.println(buffer.toString()));

        process.exitHandler(code -> {
          System.out.println("Process exited   : " + code);
        });


      })
      .start()
      .onComplete(testContext.succeedingThenComplete());
  }

  @Test
  void fromExample(Vertx vertx, VertxTestContext testContext) {
    ProcessOptions options = new ProcessOptions().setCwd("/tmp");
    Process.create(vertx, "ls", options).start();
  }
}
